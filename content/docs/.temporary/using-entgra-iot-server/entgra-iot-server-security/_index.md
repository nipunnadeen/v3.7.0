---
bookCollapseSection: true
weight: 6
---
# Entgra IoT Server Security

This section lists all the concepts, tutorials, how-to guides, and deployment guides related to Entgra IoT Server security.

### **Concepts**

*   [Application-level Security]({{< param doclink >}}key-concepts/#application-level-security)
    *   [Authentication]({{< param doclink >}}key-concepts/#authentication): [Single Sign-On (SSO)]({{< param doclink >}}key-concepts/#single-sign-on)
    *   [Authorization]({{< param doclink >}}key-concepts/#authorization): [Role-based Access Control (RBAC)]({{< param doclink >}}key-concepts/#role-based-access-control), [Scopes]({{< param doclink >}}key-concepts/#scopes)
    *   [Encryption]({{< param doclink >}}key-concepts/#encryption)
    *   [Certificates]({{< param doclink >}}key-concepts/#certificates): [Mutual SSL]({{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication)
*   [Transport-level Security]({{< param doclink >}}key-concepts/#transport-level-security)

### **Tutorials**

*   [Obtaining Access Tokens](https://docs.wso2.com/display/IoTS310/Getting+Started+with+APIs#GettingStartedwithAPIs-Obtainingtheaccesstoken)
*   [Regenerating Access Tokens](https://docs.wso2.com/display/IoTS310/Getting+Started+with+APIs#GettingStartedwithAPIs-Generatinganewaccesstokenfromtherefreshtoken)
*   [Getting Scope Details of an API](https://docs.wso2.com/display/IoTS310/Getting+the+Scope+Details+of+an+API)

### How-To Guides

*   [Resetting a User Password](https://docs.wso2.com/display/IoTS310/Resetting+a+User+Password)
*   [Changing the Super Administrator Username and Password](https://docs.wso2.com/display/IoTS310/Changing+the+Super+Administrator+Username+and+Password)
*   [Managing Client Side Mutual SSL Certificates]({{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication)
*   [Generating Certificates from the Apple Developer Portal]({{< param doclink >}}using-entgra-iot-server/working-with-ios/ios-configurations/#generating-certificates-from-the-apple-developer-portal)
*   [Adding Custom Grant Types]({{< param doclink >}}using-entgra-iot-server/entgra-iot-server-security/adding-custom-grant-types/)
*   [Configuring Keystores in Entgra IoT Server]({{< param doclink >}}using-entgra-iot-server/product-administration/Configuring-Keystores-in-Entgra-IoT-Server/)

### Deployment Guides

*   [Integrating a Third-Party Identity Provider for Access Token Management]({{< param doclink >}}using-entgra-iot-server/entgra-iot-server-security/intergrating-third-party-identity/)
