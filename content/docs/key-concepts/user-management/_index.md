---
bookCollapseSection: true
weight: 6
---

# User Management

You can create user accounts, create roles, assign permissions, and manage the devices of the users in your organization using Entgra IoT Server.

## User Roles

There are several user categories that are serviced by Entgra IoT Server:

*   **Device Owner**: These users own devices that need to be managed via Entgra IoT Server.

*   **Device Creator**: These users register devices that need to be managed via Entgra IoT Server. Depending on the organizational structure, this role might be played by a Device Admin.

*   **Device Admin**: These users perform administrative tasks related to Entgra IoT Server, such as user management, configuring security, and installing features. For more information, see [Product Administration]({{< param doclink >}}using-entgra-iot-server/product-administration/).

*   **Device Manufacturer**: These users create innovative device types that need to be managed by Entgra IoT Server. For more information, see [Device Manufacturer Guide]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/).

*   **Mobile App Creator**: These users create mobile applications using Entgra IoT Server's App Publisher. Depending on the organizational structure, this role might be played by a Mobile App Publisher. For more information on mobile app creation see the following sections:

    *   [Creating an Android Application]({{< param doclink >}}tutorials/mobile-application-management/creating-an-android-application/)

    *   [Creating an iOS Application]({{< param doclink >}}tutorials/mobile-application-management/creating-an-ios-application/)

    *   [Creating a Web Clip]({{< param doclink >}}tutorials/mobile-application-management/creating-a-web-clip/)

    *   [Creating a New Version of the Mobile Application]({{< param doclink >}}tutorials/mobile-application-management/creating-a-new-version-of-the-mobile-application/)

*   **Mobile App Publisher**: These users publish the mobile applications created by Mobile App Creators to the App Store.

For more information user role management, see [Managing Roles]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#managing-roles).


## Users

Entgra IoT Server enables creating and managing users in your organization and assigning **User Roles** to them. You can create users manually or by integrating Entgra IoT Server with an existing user store. For more information on user management, see [Managing Users](https://docs.wso2.com/display/IoTS310/Managing+Users).

## User Interfaces

Entgra IoT Server comes with the following user interfaces (UIs):

*   **Device Management Console**: This UI facilitates all the administrative tasks pertaining to Entgra IoT Server.
*   **API Store**: This UI displays all the APIs associated with Entgra IoT Server.
*   **App Publisher**: This UI enables you to create and manage mobile applications.
*   **App Store**: This UI enables you to install and update mobile applications on mobile devices. It also comes with social features such as rating and liking that help Mobile App Creators to understand the popularity and usability of their mobile applications.

