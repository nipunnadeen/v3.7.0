---
bookCollapseSection: true
weight: 7
---

# APIs

An Application Programming Interfaces (API) is a way of exposing software functionality without revealing its implementation. APIs enable software applications to interact with each other and exchange data. Following are the list of APIs Entgra IoT Server supports:

*   **Device Management APIs**: These APIs expose the device management functionality associated with Entgra IoT Server **Device Management Console**. You can also use them to facilitate device management functionality through a third-party UI as well.
*   **Device APIs**: These APIs ensure communication between devices and the Entgra IoT Server.

*   **App Management APIs**: These APIs expose app publishing and app portal functionality associated with Entgra IoT Server **App Publisher** and **App Store** respectively. You can also use them to facilitate app publishing and app portal functionality through third-party UIs as well.

*   **API Management APIs**: These APIs expose API publishing and API portal functionality associated with Entgra IoT Server. 

*   **Certificate Management APIs**: These APIs implement [Simple Certificate Enrollment Protocol (SCEP)](https://www.css-security.com/scep/) so that Entgra IoT Server can authenticate and authorize devices with SSL certificates.

For more information, see [Device Management REST APIs]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/device-management-rest-apis/).