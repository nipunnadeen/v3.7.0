---
bookCollapseSection: true
weight: 4
---

# Configurations

Entgra IoT Server enables two kinds of configurations:

*   **Preliminary configurations**: These configurations are required to manage mobile devices via Entgra IoT Server. For more information on preliminary configurations, see the following sections:

    *   [Product Administration]({{< param doclink >}}using-entgra-iot-server/product-administration/)

    *   [Working with Android Devices]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/)

    *   [Working with iOS Devices]({{< param doclink >}}using-entgra-iot-server/working-with-ios/)

    *   [Working with Windows Devices]({{< param doclink >}}using-entgra-iot-server/working-with-windows-devices/)

*   **Secondary configurations**: These configurations enable Entgra IoT Server to integrate with other applications. For more information, see the following sections: 

    *   [Configuring Entgra IoT Server with Entgra Data Analytics Server]({{< param doclink >}}using-entgra-iot-server/product-administration/Configuring-Entgra-IoT-Server-with-Entgra-Data-Analytics-Server/)

    *   [Configuring Entgra IoT Server with WSO2 Message Broker](https://docs.wso2.com/display/IoTS310/Configuring+WSO2+IoT+Server+with+WSO2+Message+Broker)

    *   [Configuring Entgra IoT Server with Entgra API Manager](https://docs.wso2.com/display/IoTS310/Configuring+WSO2+IoT+Server+with+WSO2+API+Manager)

    *   [Configuring Entgra IoT Server with Entgra Identity Server](https://docs.wso2.com/display/IoTS310/Configuring+WSO2+IoT+Server+with+WSO2+Identity+Server)

    *   [Configuring Entgra IoT Server with third-party MQTT broker]({{< param doclink >}}using-entgra-iot-server/product-administration/Configuring-Entgra-IoT-Server-with-a-Third-Party-MQTT-Broker/)

To manage IoT devices supporting HTTP, preliminary configurations are sufficient. To manage other IoT devices you need to perform secondary configurations as well.
