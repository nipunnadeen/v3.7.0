---
bookCollapseSection: true
weight: 1
---

# Legacy Android Enrollment With QR Code
Legacy enrollment is deprecated and is designed to take control of the device and data in a BYOD setting.

{{< hint info >}}
<strong>Pre-requisites</strong>
<br>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
    <li>Please follow <a href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/ "> install agent section </a></li>
    <li>Optionally, basic <a href="{{< param doclink >}}key-concepts/#android ">concepts of Android device management</a> will be beneficial as well. </li>
    <li>When you are going to enroll the device using QR code you have to set the <a 
    href="{{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/android-configurations/"> platform 
    configuration </a> of android device</li>
</ul>
{{< /   hint >}}


<iframe width="560" height="315" src="https://www.youtube.com/embed/YrDfL8IV3ro" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Open the agent application</li>
    <li>Click continue when disclaimer appears</li>
    <li>In the next screen click "Enroll with QR Code"</li>
    <li>In the server Select device ownership as "BYOD"</li>
    <li>Scan QR code that is generated in server</li>
    <li>In the device screen click "Activate"</li>
    <li>User will be prompted to agree to few permissions and click "Allow"</li>
    <li>Agree to using data usage monitoring to allow server to check the data usage of the device</li>
    <li>Allow agent to change do not disturb status which is used to ring the device</li>
    <li>Enter and confirm a PIN code, which will be needed by admin to perform any critical tasks with user concent.  Then click "Set PIN Code" to complete enrollment</li>
</ul>