---
bookCollapseSection: true
weight: 2
---

# Install Agent App

Android devices are enrolled and managed using an application that is installed on the device. This is called the device management agent app. In order to install the Entgra agent app, there are two methods,
{{< expand "Download from IoT server" "..." >}}

There is an agent app bundled with the IoT server and that can be downloaded to your mobile device and installed.

<!-- <strong>Pre-requisit</strong>
{{< expand "Server is downloaded and started" "..." >}}
<iframe src="{{< param doclink >}}guide-to-work-with-the-product/enroll-android/install-agent/" onload="this.width=screen.width;this.height=screen.height;">html-name</iframe>
{{< /expand >}} -->

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
</ul>
{{< /hint >}}

### Step 1  
Click "+ Add" button under Device tab

![image](../../../../image/2000.png)

### Step 2
Click on "Android" under device types

![image](../../../../image/2001.png)

### Step 3
Click on "Get Android agent" under device types

![image](../../../../image/2002.png)

### Step 4
Scan the QR code using the mobile device. New Android OS version include QR code scanning app inbuilt. If this is not available, a QR code scanning app can be downloaded from Playstore. Follow the QR code scanned link to download the agent to the device.

Alternatively, This page can be assed via the mobile device browser's itself(Do step 1-3 in the phone's broswer) and click on "Download APK" to download the APK to the device.

### Step 5

Once the app is downloaded, click open. Depending on the OS version of the device, unknown sources has to be enabled to install the agent. In Android 8 and upwards, once open is clicked, following screen will be displayed and please tick "Allow from this source" and go back.

![image](../../../../image/2004.jpg)

### Step 6

Click install to install the agent

![image](../../../../image/2005.jpg)

{{< /expand >}}
{{< expand "Download from playstore" "..." >}}
## Download from playstore

{{< hint info >}}
<strong>Pre-requisit</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
</ul>
{{< /hint >}}

Visit the link https://play.google.com/store/apps/details?id=io.entgra.iot.agent using your mobile device or go to Google play store app in the mobile device and search for "Entgra Device Management Agent" and install the app.
{{< /expand >}}