---
bookCollapseSection: true
weight: 7
---

# <strong> LDAP Settings </strong>

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to define settings for connecting to LDAP servers. Once this 
configuration profile is installed on an iOS device, corresponding users will not be able to modify 
these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>Display name of the account
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname</strong></td>
            <td>LDAP Host name or IP address</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication.</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>User name for this LDAP account</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>Password for this LDAP account</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Search Settings </strong>
                    <br> Search settings for this LDAP account. Can have many of these for one account. Should have at least one for the account to be useful.</center>
            </td>
        </tr>
        <tr>
            <td><strong>Description</strong></td>
            <td>Description of this search setting</td>
        </tr>
        <tr>
            <td><strong>Search Base</strong></td>
            <td> Conceptually, the path to the node where a search should start. For example: ou=people,o=example corp
            </td>
        </tr>
        <tr>
            <td><strong>Scope</strong></td>
            <td>Defines what recursion to use in the search. Can be one of the following 3 values:
                <ul style="list-style-type:disc;">
                    <li>LDAPSearchSettingScopeBase: Just the immediate node pointed to by SearchBase
                    </li>
                    <li>LDAPSearchSettingScopeOneLevel: The node plus its immediate children .
                    </li>
                    <li>LDAPSearchSettingScopeSubtree: The node plus all children, regardless of depth.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}