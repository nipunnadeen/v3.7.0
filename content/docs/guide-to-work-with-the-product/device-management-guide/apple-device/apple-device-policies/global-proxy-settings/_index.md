---
bookCollapseSection: true
weight: 3
---

# <strong> Global Proxy Settings </strong>

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

Configure a global HTTP proxy to direct all HTTP traffic from Supervised iOS 7 and higher devices through a designated proxy server. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server

<i>This policy is only applicable for the devices enrolled in supervised mode.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Proxy Configuration Type</strong></td>
            <td>If you choose manual proxy type, you need the proxy server
                address including its port and optionally a username and
                password into the proxy server. If you choose auto proxy type,
                you can enter a proxy autoconfiguration (PAC) URL.
            </td>
        </tr>
        <tr>
            <td><strong>Proxy Host</strong></td>
            <td>The proxy serverʼs network address.(Host name/IP address of the proxy server.)</td>
        </tr>
        <tr>
            <td><strong>Proxy Port</strong></td>
            <td>The proxy serverʼs port</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td> The username used to authenticate to the proxy
                server.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The password used to authenticate to the proxy server</td>
        </tr>
        <tr>
            <td><strong>Allow Captive Login</strong></td>
            <td>When checked, Allows the device to bypass the proxy server to display the login page for captive networks.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}