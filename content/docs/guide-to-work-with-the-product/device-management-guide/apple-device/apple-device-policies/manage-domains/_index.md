---
bookCollapseSection: true
weight: 6
---

# <strong> Manage Domains </strong>

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This payload defines web domains that are under an enterprise’s management.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
                <th><strong>Data keys of Policy</strong></th>
                <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Unmarked Email Domains</strong>
                    <br> (Any email address that does not have a suffix that matches one of the unmarked email domains specified by the key EmailDomains will be considered out-of-domain and will be highlighted as such in the Mail app.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Email Domains</strong></td>
            <td>An array of strings. An email address lacking a suffix that matches any of these strings will be considered out-of-domain.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Managed Safari Web Domains</strong>
                    <br>(Supervised only. If present, only AirPlay destinations present in this list are available to the device.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Managed Safari Web Domains</strong></td>
            <td>An array of URL strings. URLs matching the patterns listed here will be considered managed.
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}